from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser, JSONParser
from api.serializers import FileSerializer, UserPicRequestSerializer
from rest_framework.response import Response
from rest_framework import status
from .models import Profile

class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):

      file_serializer = FileSerializer(data=request.data)

      if file_serializer.is_valid():
          file_serializer.save()
          return Response(file_serializer.data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class  ReturnStaticImageFilePathByUsername(APIView):
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
      serializer = UserPicRequestSerializer(data=request.data)
      if serializer.is_valid():
          userid = User.objects.filter(username = request.data['username']).first()
          profile = Profile.objects.filter(user_id = userid).first()
          if profile is not None:
              return Response(profile.profile_pic.name, status=status.HTTP_200_OK)
          else:
              return Response(serializer.errors, status=status.HTTP_204_NO_CONTENT)
      return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)