from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils import timezone
from datetime import datetime    

# Create your models here.


class File(models.Model):
    '''
    Подробная Документация на модель File будет тут
    '''
    file = models.FileField(blank=True, null=True, upload_to = 'files/')
    def __str__(self):
        return self.file.name


class Profile(models.Model):
    '''
    Подробная Документация на модель Profile будет тут
    '''
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, null=True, blank=True)
    birth_date = models.DateTimeField(null=True, blank=True)
    #добавлено дублирование файла в общие - через File class
    profile_pic = models.ImageField(upload_to='profile_pics',blank=True)

    def __str__(self):
      return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    '''
    ресивер для синхронизации создания модели Profile при создании Пользователя
    '''
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    '''
    ресивер для синхронизации изменения модели Profile при изменении пользователя
    '''
    instance.profile.save()