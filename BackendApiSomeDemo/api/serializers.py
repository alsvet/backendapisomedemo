import hashlib

from .models import File, Profile
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_auth.registration.serializers import RegisterSerializer


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"

class UserPicRequestSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    class Meta:
        model = User
        fields = ['username']

class CustomRegisterSerializer(RegisterSerializer):

    first_name = serializers.CharField()
    last_name = serializers.CharField()
    birth_date = serializers.DateField()  
    agree = serializers.BooleanField()
    photo = serializers.FileField()

    username = serializers.CharField(
        max_length=21,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    email = serializers.EmailField(
        max_length=100,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    location = serializers.CharField(
        max_length=100,
    )

    def validate(self, data):
        username = self.initial_data['username']
        if len(username) >= 21 or len(username) <=7:
            raise serializers.ValidationError("Your username is to big or too small, please consider it  greater then 7 or shorten it to 20 letters")
        agree = data['agree']
        if agree == False:
            raise serializers.ValidationError("Please read and agree to Terms of Service")
        first_name = self.initial_data['first_name']
        if len(first_name) >= 60:
            raise serializers.ValidationError("Your Firstname is to big, please shorten it to 60 letters")
        if (first_name.isalpha() == False):
            raise serializers.ValidationError("Your Firstname cannot contain numbers, please type letters only")
        last_name = self.initial_data['last_name']
        if len(last_name) >= 60:
            raise serializers.ValidationError("Your Lastname is to big, please shorten it to 60 letters")
        if (last_name.isalpha() == False):
            raise serializers.ValidationError("Your Lastname cannot contain numbers, please type letters only")
        return data


    def get_cleaned_data(self):
        super(CustomRegisterSerializer, self).get_cleaned_data()
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'location': self.validated_data.get('location', ''),
            'birth_date': self.validated_data.get('birth_date', ''),
            'agree': self.validated_data.get('agree', ''),
            'photo': self.validated_data.get('photo', ''),
        }


    def save(self, request):
        user = super(CustomRegisterSerializer, self).save(request)
        profile_pic = File(file = request.FILES['photo'])
        user.profile.profile_pic = self.cleaned_data['photo']
        user.profile.location = self.cleaned_data['location']
        user.profile.birth_date = self.cleaned_data['birth_date']
        profile_pic.save()
        user.profile.save()
        return user