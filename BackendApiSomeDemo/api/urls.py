from django.urls import path, include, re_path
from api import views

urlpatterns = [
    #path('users/', include('users.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('fileupload/', views.FileUploadView.as_view()),
    path('getpicpath/', views.ReturnStaticImageFilePathByUsername.as_view()),

]
