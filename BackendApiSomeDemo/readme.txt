﻿BackendApiSomeDemo
 	    	 

 	  notes:  	 
 	DEBUG only	 
 	no production only	 
 	demo only	 
 		 
 	  стек:  	 
 	python 3.7	 
 	django 2.2.1	 
 	sqllite	 
 	bootstrap	 
 		 
 	  IDEs:  	 
 	VS2019 16.2.0	 
 	Jetbrains DG 2018.2.4	 
 		 
 	  сетап:  	 
 	 $ cd BackendApiSomeDemo	 
 	 $ python3.7 -m venv .env 	 
 	 $ source .env/bin/activate	 
 	 $ pip install django	 
 	 $ pip install -r requirements.txt	 
 	 $ mkdir files	 
 	 $ mkdir profile_pics	 
 	 $ chmod 777 manage.py	 
 	 $ ./manage.py collectstatic	 
 	 $ ./manage.py runserver	 
 		 
 	 	 
 	  реализованны 2 джанго аппа  	 
 	api - API	 
 	BackendApiSomeDemo - фронт с несколькими view	 
 		 
 	апи рут:	 
 	/api/v1/	 
 		 
 	  реализованные апи функции:  	 
 	/api/v1/fileupload/	 
 	/api/v1/rest-auth/registration/	 
 	/api/v1/ReturnStaticImageFilePathByUsername/	 
 		 
 	шаблонные:	 
 	api/v1/ rest-auth/ ^password/reset/$ [name='rest_password_reset']	 
 	api/v1/ rest-auth/ ^password/reset/confirm/$ [name='rest_password_reset_confirm']	 
 	api/v1/ rest-auth/ ^login/$ [name='rest_login']	 
 	api/v1/ rest-auth/ ^logout/$ [name='rest_logout']	 
 	api/v1/ rest-auth/ ^user/$ [name='rest_user_details']	 
 	api/v1/ rest-auth/ ^password/change/$ [name='rest_password_change']	 
 		 
 		 
 		 
 		 
 	  реализованные в BackendApiSomeDemo view:  	 
 	шаблонные:	 
 	/home/	 
 	/contact/	 
 	/about/	 
 	/login/	 
 	/logout/	 
 		 
 		 
 		 
 		 
 	сахар:  	 
 	реализован тротлинг в апи 	 
 	#REST_CONFIG	 
 	    'DEFAULT_THROTTLE_RATES': {	 
 	        'anon': '1000/day',	 
 	        'user': '10000/day',	 
 	        'API': '20000/day',	 
 	    },	 
 		 
 	Созданы доки на модели(авторизоватся админом):	 
 	/admin/doc/	 
 	пример:	 
 	/admin/doc/models/api.file/	 
 		 
 		 
 	добавлен allauth - для возможной дальнейшей авторизации по сторонним соцсетям	 
 		 
 		 
 	валидаторы при регистрации	 
 	username = self.initial_data['username']	 
 	        if len(username) >= 21 or len(username) <=7:	 
 	            raise serializers.ValidationError("Your username is to big or too small, please consider it  greater then 7 or shorten it to 20 letters")	 
 	        agree = data['agree']	 
 	        if agree == False:	 
 	            raise serializers.ValidationError("Please read and agree to Terms of Service")	 
 	        first_name = self.initial_data['first_name']	 
 	        if len(first_name) >= 60:	 
 	            raise serializers.ValidationError("Your Firstname is to big, please shorten it to 60 letters")	 
 	        if (first_name.isalpha() == False):	 
 	            raise serializers.ValidationError("Your Firstname cannot contain numbers, please type letters only")	 
 	        last_name = self.initial_data['last_name']	 
 	        if len(last_name) >= 60:	 
 	            raise serializers.ValidationError("Your Lastname is to big, please shorten it to 60 letters")	 
 	        if (last_name.isalpha() == False):	 
 	            raise serializers.ValidationError("Your Lastname cannot contain numbers, please type letters only")	 
 	- проверка длины username	 
 	- ограничение длин имени и фамилии	 
 	- обязательное соглашение с условиями пользования.	 
 		 
 		 
 	  продемонстрированно:  
 	- знание джанго/питон	 
 	- знание джанго-рест-фреймворк и разработки API	 
 	- class based views/function based views	 
 	- интегрирование и структурирование приложений и кода в них в пределах одного проекта	 
 	- умение работы с ORM и SQL напрямую.  
 	- умение работать с git и системами управления версиями	 
 	- умение документировать код	 
 	- умение принимать архитектурные и солюшен решения при проектировании	 
 	- умение принимать превентивне решения по безопасности	 
 	- industry-standard level code/solution developpment	 
 	 	 
 		 
 	трудозатраты: 5 часов	 

 v0.0.2 
  добавлены 3 пользователя: testuser1, testuser2, testuser3 : Pass123$ 
  суперюзер: admin:1q2w3e4r 
  
