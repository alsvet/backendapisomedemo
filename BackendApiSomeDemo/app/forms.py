"""
Definition of forms.
"""

from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))




class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField(label=_("Email address"), required=True, help_text=_("Required."), widget=forms.TextInput({
        'class':'card__field-input',
        'id':'email',
        'placeholder':'youremail@somehost.com'}))
    password1 = forms.CharField(label='Password', required=True, widget=forms.PasswordInput({
        'id':'password1',
        'class':'card__field-input',
        'placeholder':'Password'}))
    password2 = forms.CharField(label='Repeat password', required=True, widget=forms.PasswordInput({
        'id':'password2',
        'class':'card__field-input',
        'placeholder':'Repeat Password'}))

    agree = forms.BooleanField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2', 'agree')
        widgets = {
            'agree' : forms.CheckboxInput(attrs={
                'class':'card__field-input-cb',
                'id':'agree'             
            }),

            'last_name': forms.TextInput(attrs={
                'class':'card__field-input',
                'placeholder': 'Last Name',
                'id':'second_name'
            }),
            'first_name': forms.TextInput(attrs={
                'class':'card__field-input',
                'placeholder': 'First Name',
                'id':'first_name'
            }),
            'username': forms.TextInput(attrs={
                'class':'card__field-input',
                'placeholder': 'Username',
                'id':'username'
            }),
            'email': forms.TextInput(attrs={
                'class':'card__field-input',
                'placeholder': 'Email',
                'id':'email'
            }),

        }

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.username = self.cleaned_data["username"]
        user.password = self.cleaned_data["password1"]
        if commit:
            user.save()
        return user

    def clean_agree (self, *args, **kwargs):
        agree = self.cleaned_data.get('agree')
        if agree == False:
            raise forms.ValidationError("Please read and agree to Terms of Service")
        else:
            return agree

    def clean_username (self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        if len(username) >= 21 or len(username) <=7:
            raise forms.ValidationError("Your username is to big or too small, please consider it  greater then 7 or shorten it to 20 letters")
        username_qs = User.objects.filter(username=username)
        if username_qs.exists():
            raise forms.ValidationError("Username already exists")
        else:
            return username

    def clean_first_name (self, *args, **kwargs):
        first_name = self.cleaned_data.get('first_name')
        if len(first_name) >= 60:
            raise forms.ValidationError("Your Firstname is to big, please shorten it to 60 letters")
        if (first_name.isalpha() == False):
            raise forms.ValidationError("Your Firstname cannot contain numbers, please type letters only")
        else:
            return first_name

    def clean_last_name (self, *args, **kwargs):
        last_name = self.cleaned_data.get('last_name')
        if len(last_name) >= 60:
            raise forms.ValidationError("Your Lastname is to big, please shorten it to 60 letters")
        if (last_name.isalpha() == False):
            raise forms.ValidationError("Your Lastname cannot contain numbers, please type letters only")
        else:
            return last_name

    def clean(self):
        cleaned_data = super(UserCreationForm, self).clean()
        return cleaned_data