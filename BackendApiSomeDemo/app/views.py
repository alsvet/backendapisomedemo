"""
Definition of views.
"""

from rest_framework.response import Response
from rest_framework import status
from datetime import datetime
from django.shortcuts import render
from django.http import HttpRequest

from .forms import UserRegistrationForm

from api.serializers import CustomRegisterSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Добро пожаловать',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'8-800-2000-600',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Описание Аппа фронта будет тут',
            'year':datetime.now().year,
        }
    )
